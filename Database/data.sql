-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: auctiondb
-- ------------------------------------------------------
-- Server version	5.7.18-enterprise-commercial-advanced-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `bidhistory`
--

DROP TABLE IF EXISTS `bidhistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bidhistory` (
  `ProID` int(11) NOT NULL,
  `uID` int(11) NOT NULL,
  `Price` int(50) DEFAULT NULL,
  `Time` datetime NOT NULL,
  `uName` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ProID`,`uID`,`Time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bidhistory`
--

LOCK TABLES `bidhistory` WRITE;
/*!40000 ALTER TABLE `bidhistory` DISABLE KEYS */;
INSERT INTO `bidhistory` VALUES (1,11,2600000,'2017-06-24 04:48:53','1312399'),(3,11,10002000,'2017-06-22 18:43:07','1312399'),(3,12,10003000,'2017-06-23 03:36:44','User1'),(4,11,104000,'2017-06-22 18:41:51','1312399'),(4,11,105000,'2017-06-22 19:11:51','1312399'),(4,11,106000,'2017-06-22 19:16:25','1312399'),(4,11,107000,'2017-06-22 19:16:28','1312399'),(6,11,104000,'2017-06-22 18:56:16','1312399'),(6,11,5100000,'2017-06-22 20:41:28','1312399'),(9,11,3100000,'2017-06-22 20:41:13','1312399'),(10,11,1001000,'2017-06-22 23:11:25','1312399'),(11,11,800000,'2017-06-24 03:45:45','1312399'),(12,11,240000,'2017-06-24 03:45:13','1312399'),(14,11,10001000,'2017-06-22 20:41:48','1312399'),(14,11,10002000,'2017-06-22 20:41:58','1312399'),(14,11,18100000,'2017-06-22 20:42:01','1312399'),(18,11,901000,'2017-06-22 23:43:05','1312399'),(23,11,1900001000,'2017-06-24 04:49:05','1312399'),(24,13,1900100000,'2017-06-24 04:42:51','User12'),(25,13,1200100000,'2017-06-24 04:20:58','User12');
/*!40000 ALTER TABLE `bidhistory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `CatID` int(11) NOT NULL AUTO_INCREMENT,
  `CatName` varchar(45) NOT NULL,
  `CatDes` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`CatID`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'Điện thoại ','Những sản phẩm là bao gồm điện thoại và có liên quan đến điện thoại.'),(2,'Đồ gia dụng','Những vật dụng, mặt hàng, thiết bị được trang bị và sử dụng để phục vụ cho các tiện nghi, tiện ích nhằm đáp ứng nhu cầu sử dụng thường xuyên cho sinh hoạt hàng ngày.'),(3,'Phụ kiện - Thiết bị số','Những sản phẩm như: headphone, tai nghe, sạc dự phòng, gậy selfie...'),(4,'Thời trang','Những sản phẩn như quần, áo ,giày, dép...'),(5,'Bất động sản','Sản phẩm đấu giá ở đây là nhà đất.'),(11,'Khác',NULL);
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `ProID` int(11) NOT NULL AUTO_INCREMENT,
  `ProName` varchar(45) DEFAULT NULL,
  `CatID` int(11) DEFAULT NULL,
  `NowPrice` int(50) DEFAULT NULL,
  `Numbit` int(11) DEFAULT '0',
  `Starttime` datetime DEFAULT NULL,
  `Endtime` datetime DEFAULT NULL,
  `TinyDes` varchar(100) DEFAULT NULL,
  `FullDes` varchar(100) DEFAULT NULL,
  `uID` int(11) DEFAULT NULL,
  `Ownid` int(11) DEFAULT NULL,
  `Sellprice` int(50) unsigned DEFAULT '0',
  `IsAutotime` int(11) DEFAULT NULL,
  `IsEnd` int(11) DEFAULT '0',
  `ProStep` int(11) DEFAULT '1000',
  PRIMARY KEY (`ProID`),
  KEY `userID_idx` (`uID`),
  KEY `proOwnid_idx` (`Ownid`),
  KEY `catID_idx` (`CatID`),
  CONSTRAINT `proOwnid` FOREIGN KEY (`Ownid`) REFERENCES `user` (`uID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `userID` FOREIGN KEY (`uID`) REFERENCES `user` (`uID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (1,'Bphone',1,2600000,0,'2017-06-01 23:00:00','2017-09-27 00:00:00','Nothing','Bphone xịn',12,11,2600000,NULL,1,1000),(2,'Iphone 7 Plus',1,15000000,0,'2017-06-03 00:00:00','2017-10-10 00:00:00','Nothing','Iphone 7 còn mới chưa dùng',12,11,15100000,NULL,0,1000),(3,'Samsung Galaxy S8',1,10003000,10,'2017-06-06 00:00:00','2017-09-27 00:00:00','Nothing','S8 xách tay từ Iraq về',12,12,10100000,NULL,0,1000),(4,'LG V10',1,107000,4,'2017-06-15 00:00:00','2017-09-27 00:00:00','Nothing','LGV10 mới nhập từ chợ Lớn về HongKog',12,11,8100000,NULL,0,1000),(5,'Sony Z5',1,18000000,0,'2017-06-10 00:00:00','2017-09-27 00:00:00','Nothing',NULL,NULL,NULL,18100000,NULL,0,1000),(6,'Tủ Lạnh Sanio',2,5100000,5,'2017-06-14 00:00:00','2017-09-27 00:00:00','Nothing',NULL,12,11,5100000,NULL,1,1000),(7,'Máy Xay Sinh Tố PhiLips',2,800000,0,'2017-06-15 00:00:00','2017-09-27 00:00:00','Nothing',NULL,NULL,NULL,900000,NULL,0,1000),(8,'Quạt Đứng',2,10000,3,'2017-06-15 00:00:00','2017-09-27 00:00:00','Nothing','12',12,11,1300000,NULL,1,1000),(9,'Ghế Giám Đốc',2,3100000,0,'2017-06-17 00:00:00','2017-09-27 00:00:00','Nothing',NULL,12,11,3100000,NULL,1,1000),(10,'Bàn Học Trẻ Em',2,1001000,0,'2017-06-19 00:00:00','2017-09-27 00:00:00','Nothing',NULL,13,11,1100000,NULL,0,1000),(11,'Tai Nghe',3,800000,5,'2017-06-19 00:00:00','2017-09-27 00:00:00','Nothing',NULL,13,11,800000,NULL,1,1000),(12,'Pin Dự Phòng',3,240000,0,'2017-06-15 00:00:00','2017-09-27 00:00:00','Nothing',NULL,13,11,240000,NULL,1,1000),(13,'Tai Nghe Nhét Tai',3,220000,0,'2017-06-14 00:00:00','2017-09-27 00:00:00','Nothing',NULL,NULL,NULL,320000,NULL,0,1000),(14,'Macbook',3,18100000,1,'2017-06-17 00:00:00','2017-09-27 00:00:00','Nothing',NULL,12,11,18100000,NULL,1,1000),(15,'IPad Mini',3,10000000,0,'2017-06-17 00:00:00','2017-09-27 00:00:00','Nothing',NULL,NULL,NULL,10100000,NULL,0,1000),(16,'Son môi Maybelline',4,300000,0,'2017-06-15 00:00:00','2017-09-27 00:00:00','Nothing',NULL,NULL,NULL,400000,NULL,0,1000),(17,'Son môi Mint Cosmetics',4,400000,0,'2017-06-15 00:00:00','2017-09-27 00:00:00','Nothing',NULL,NULL,NULL,500000,NULL,0,1000),(18,'Áo Khoác Lông Thú',4,901000,11,'2017-06-17 00:00:00','2017-09-27 00:00:00','Nothing',NULL,NULL,11,1000000,NULL,0,1000),(19,'Vòng Cổ',4,350000,0,'2017-06-17 00:00:00','2017-09-27 00:00:00','Nothing',NULL,NULL,NULL,450000,NULL,0,1000),(20,'Vòng Tay',4,100000,0,'2017-06-19 00:00:00','2017-09-27 00:00:00','Nothing',NULL,NULL,11,650000,NULL,0,1000),(21,'Nhà Đất quận 8',5,1800000000,0,'2017-06-14 00:00:00','2017-06-28 00:00:00','Nothing',NULL,NULL,NULL,1800100000,NULL,0,1000),(22,'Bitexco Quận 1',5,100000,0,'2017-06-15 00:00:00','2017-06-27 00:00:00','Nothing',NULL,NULL,11,1200100000,NULL,0,1000),(23,'Chung cư quận 5',5,1900001000,0,'2017-06-15 00:00:00','2017-09-27 00:00:00','Nothing',NULL,NULL,11,1900100000,NULL,0,1000),(24,'Dãy nhà trọ thủ đức',5,1900100000,0,'2017-06-15 00:00:00','2017-06-29 00:00:00','Nothing',NULL,12,13,1900100000,NULL,1,1000),(25,'Nhà quận 10',5,1200100000,0,'2017-06-17 00:00:00','2017-06-29 00:00:00','Nothing',NULL,12,13,1200100000,NULL,1,1000);
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ratelist`
--

DROP TABLE IF EXISTS `ratelist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ratelist` (
  `uID` int(11) DEFAULT NULL,
  `uID2` int(11) DEFAULT NULL,
  `Point` int(11) DEFAULT NULL,
  `Comment` varchar(100) DEFAULT NULL,
  `uName` varchar(45) DEFAULT NULL,
  `rateID` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`rateID`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ratelist`
--

LOCK TABLES `ratelist` WRITE;
/*!40000 ALTER TABLE `ratelist` DISABLE KEYS */;
INSERT INTO `ratelist` VALUES (11,12,1,'asdasd','1312399',2),(11,12,0,'Sad','1312399',3),(11,12,1,'Good','1312399',4),(11,12,1,'asdasd','1312399',5),(12,11,1,'Good','User1',6),(11,12,1,'Pin du phong','1312399',7),(11,12,1,'sda','1312399',8),(11,12,1,'asdsa','1312399',9),(11,13,1,'','1312399',10),(11,13,1,'asdasd','1312399',11),(12,13,1,'Good','User1',12),(11,13,1,'ádasd','1312399',13),(11,12,1,'Đẹp','1312399',14),(13,12,1,'Good','User12',15);
/*!40000 ALTER TABLE `ratelist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sessions` (
  `session_id` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `expires` int(11) unsigned NOT NULL,
  `data` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sessions`
--

LOCK TABLES `sessions` WRITE;
/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;
INSERT INTO `sessions` VALUES ('gGfwJNHEIek8hUvPIraKHCuBzHafNVOY',1497784827,'{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"httpOnly\":true,\"path\":\"/\"},\"isLogged\":false}');
/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `uID` int(11) NOT NULL AUTO_INCREMENT,
  `uName` varchar(20) NOT NULL,
  `uPassword` char(60) DEFAULT NULL,
  `uEmail` varchar(45) DEFAULT NULL,
  `uPoint` double DEFAULT '0',
  `uLikelist` varchar(45) DEFAULT NULL,
  `uType` int(11) DEFAULT NULL,
  `IsRight` int(11) DEFAULT NULL,
  `ratenum` int(11) DEFAULT '0',
  PRIMARY KEY (`uID`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (11,'1312399','c4ca4238a0b923820dcc509a6f75849b','nguyentrongnhan1895@gmail.com',1,NULL,1,1,1),(12,'User1','c4ca4238a0b923820dcc509a6f75849b','1@gmail.com',8,NULL,0,1,9),(13,'User12','c4ca4238a0b923820dcc509a6f75849b','123123@asdad',4,NULL,0,1,4);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `watchlist`
--

DROP TABLE IF EXISTS `watchlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `watchlist` (
  `uID` int(11) NOT NULL,
  `ProID` int(11) NOT NULL,
  `ProName` varchar(45) DEFAULT NULL,
  `NowPrice` int(50) DEFAULT NULL,
  PRIMARY KEY (`uID`,`ProID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `watchlist`
--

LOCK TABLES `watchlist` WRITE;
/*!40000 ALTER TABLE `watchlist` DISABLE KEYS */;
INSERT INTO `watchlist` VALUES (11,2,'Iphone 7 Plus',15000000),(11,4,'LG V10',8000000),(11,7,'Máy Xay Sinh Tố PhiLips',800000),(11,19,'Vòng Cổ',350000),(11,20,'Vòng Tay',550000),(11,23,'Chung cư quận 5',1900001000),(11,25,'Nhà quận 10',1200000000),(12,3,'Samsung Galaxy S8',10002000);
/*!40000 ALTER TABLE `watchlist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'auctiondb'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-06-28 12:02:55
