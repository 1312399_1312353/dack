var express = require('express'),
    handlebars = require('express-handlebars'),
    handlebars_sections = require('express-handlebars-sections'),
    bodyParser = require('body-parser'),
    cookieParser = require('cookie-parser'),
    morgan = require('morgan'),
    path = require('path'),
    wnumb = require('wnumb'),
    handleLayout = require('./middle-wares/handleLayout'),
    handle404 = require('./middle-wares/handle-404'),
    homeController = require('./controllers/homeController'),
    categoryController = require('./controllers/categoryController'),
    productController = require('./controllers/productController');
    userController=require('./controllers/userController');
    session  = require('express-session');
    moment=require('moment');
    var back = require('express-back');



var app = express();

var    passport = require('passport');
var    flash    = require('connect-flash');
       require('./models/passport')(passport);
       var DateFormats = {
              short: "DD MMMM - YYYY",
              long: "dddd DD.MM.YYYY HH:mm"
       };

app.use(morgan('dev'));
app.use(cookieParser()); // read cookies (needed for auth)

app.engine('hbs', handlebars({
    extname: 'hbs',
    defaultLayout: 'main',
    layoutsDir: 'views/_layouts/',
    partialsDir: 'views/_partials/',
    helpers: {
        section: handlebars_sections(),
        number_format: function (n) {
            var nf = wnumb({
                thousand: ','
            });
            return nf.to(n);},
        formatDate:  function(datetime, format) {
          if (moment) {
    // can use other formats like 'lll' too
          format = DateFormats[format] || format;
            return moment(datetime).format(format);
            }
              else {
                return datetime;
              }
            },
          if_eq:function(a, b, opts) {
            if (a == b) {
              return opts.fn(this);
            } else {
              return opts.inverse(this);
            }
          }
          }
}));
app.set('view engine', 'hbs');
app.use(express.static(
    path.resolve(__dirname, 'public')
));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(session({
	secret: 'vidyapathaisalwaysrunning',
	resave: true,
	saveUninitialized: true
 } )); // session secret
 app.use(back());
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash()); // use connect-flash for flash messages stored in session
// routes ======================================================================

app.use(handleLayout);
//app.use('/', homeController);
require('./controllers/homeController.js')(app, passport);
app.use('/category', categoryController);
app.use('/product', productController);
app.use('/user', userController);
app.use(handle404);

app.listen(process.env.PORT || 3000);
