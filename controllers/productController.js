var express = require('express'),
    productRepo = require('../models/productRepo');
    categoryRepo = require('../models/categoryRepo');
var r = express.Router();
// Bycat
function loadAllByCat(req, res,next) {
  var catId = req.params.id;
    if (!catId) {
        res.redirect('/');
        }
        productRepo.loadAllByCat(catId)
        .then(function(pRows){
          if (pRows.length!=0)
          {
          layoutVM: res.locals.layoutVM,
              req.products=pRows;
              req.noProducts=pRows==0;
              return next();
            }
        res.send("No Products");
       });
}
function loadCat(req, res,next) {
  var catId = req.params.id;
    if (!catId) {
        res.redirect('/');
        }
        categoryRepo.loadDetail(catId)
        .then(function(cat){
              req.category=cat;
              next();
       });
}
function loadAllCat(req, res,next) {
  categoryRepo.loadAll()
      .then(function(rows) {
              req.categories= rows;
              next();
      }).fail(function(err) {
          console.log(err);
          res.end('fail');
      });
}
function loadnotAllPro(req, res,next) {
  productRepo.loadnotAll()
      .then(function(rows) {
              req.products= rows;
              next();
      }).fail(function(err) {
          console.log(err);
          res.end('fail');
      });
}
function loadProDetail(req, res,next) {
  var ProID = req.params.id;
    if (!ProID) {
        res.redirect('/');
        }
        productRepo.loadDetail(ProID)
        .then(function(pro){
              req.product=pro;

              next();
       });
}
function loadHistory(req, res,next) {
  var ProID = req.params.id;
    if (!ProID) {
        res.redirect('/');
        }
        productRepo.loadHistory(ProID)
        .then(function(his){
              req.history=his;
              console.log(his==0);
              next();
            }).fail(function(err) {
                console.log(err);
                res.end('fail');
            });
}
function renderWaitingPage(req, res) {
    res.render('product/product', {
        layoutVM:  res.locals.layoutVM,
        categories: req.categories,
        product:req.products
    });
}
function renderAddPage(req,res){
  res.render('product/add', {
      layoutVM:  false,
  });
}
function renderbycatPage(req, res) {
    res.render('product/category', {
        layoutVM:  false,
        category:  req.category,
        products:  req.products,
        categories: req.categories,
        noProducts:req.noProducts
    });
}
function renderprodetailPage(req, res) {
  res.render('product/detail', {
      layoutVM:  false,
      category:  req.category,
      categories: req.categories,
      product:  req.product,
      histories:req.history,
      noProducts:req.noProducts
  });
}
function simplesearch(req, res,next) {
  productRepo.searchbyName(req.query.ProName)
      .then(function(rows) {
              req.SearchName=req.query.ProName;
              req.products= rows;
              next();
      }).fail(function(err) {
          console.log(err);
          res.end('fail');
      });
}
function fixsearch(req, res,next) {
  productRepo.fixsearch(req.query.ProName,req.query.selectID)
      .then(function(rows) {
              req.SearchName=req.query.ProName;
              req.products= rows;
              next();
      }).fail(function(err) {
          console.log(err);
          res.end('fail');
      });
}
function rendersearchPage(req, res) {
    res.render('product/searchresult', {
        layoutVM:  false,
        SearchName:req.SearchName,
        categories: req.categories,
        products:  req.products
    });
}
r.get('/fixsearch',loadAllCat, fixsearch,rendersearchPage);
r.get('/simplesearch',loadAllCat, simplesearch,rendersearchPage);
r.get('/bycat/:id', loadAllByCat,loadCat,loadAllCat,renderbycatPage);
r.get('/detail/:id',loadHistory, loadProDetail,loadCat,loadAllCat,renderprodetailPage);
r.post('/confirm', function(req, res) {
    productRepo.confirm(req.body).then(function(affectedRows) {
        res.redirect('/product');
    })
});
r.post('/add', function(req, res) {
    productRepo.insert(req.body);
   res.back();
});
r.post('/updatedes', function(req, res) {
    userRepo.updatedes(req.body);
   res.back();
});
r.get('/',loadAllCat,loadnotAllPro,renderWaitingPage);
module.exports = r;
