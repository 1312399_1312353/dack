var express = require('express');
    userRepo=require('../models/userRepo');
    productRepo=require('../models/productRepo');
    categoryRepo=require('../models/categoryRepo')
var r = express.Router();
function loadbyEndtime(req, res,next) {
  productRepo.loadbyEndtime()
      .then(function(rows) {
              req.endpro= rows;
              next();
      }).fail(function(err) {
          console.log(err);
          res.end('fail');
      });
}
function loadbyNumbit(req, res,next) {
  productRepo.loadbyNumbit()
      .then(function(rows) {
              req.hotpro= rows;
              next();
      }).fail(function(err) {
          console.log(err);
          res.end('fail');
      });
}
function loadbyNowPrice(req, res,next) {
  productRepo.loadbyNowPrice()
      .then(function(rows) {
              req.expensivepro= rows;
              next();
      }).fail(function(err) {
          console.log(err);
          res.end('fail');
      });
}
function loadAllCat(req, res,next) {
  categoryRepo.loadAll()
      .then(function(rows) {
              req.categories= rows;
              next();
      }).fail(function(err) {
          console.log(err);
          res.end('fail');
      });
}
function renderhomePage(req, res) {
  res.render('home/index', {
      layoutVM:  res.locals.layoutVM,
      endpro:    req.endpro,
      hotpro:  req.hotpro,
      categories:req.categories,
      expensivepro: req.expensivepro
  });
}

module.exports = function(app, passport) {
  app.get('/',loadbyEndtime,loadbyNumbit,loadbyNowPrice,loadAllCat,renderhomePage);
  app.get('/login', function(req, res) {
    res.render('home/login', { message: req.flash('loginMessage') });
  });
  app.post('/login', passport.authenticate('local-login', {
            successRedirect : '/',
            failureRedirect : '/login',
            failureFlash : true // allow flash messages
    }),
        function(req, res) {
            console.log("hello");

            if (req.body.remember) {
              req.session.cookie.maxAge = 1000 * 60 * 3;
            } else {
              req.session.cookie.expires = false;
            }
        res.redirect('/');
    });
  app.get('/register', function(req, res) {
    res.render('home/register', { message: req.flash('registerMessage') });
  });
  app.post('/register', passport.authenticate('local-register', {
		successRedirect : '/', // redirect to the secure profile section
		failureRedirect : '/register', // redirect back to the signup page if there is an error
		failureFlash : true // allow flash messages
	}));
  app.get('/logout', function(req, res) {
  		req.logout();
  		res.redirect('/');
  	});
};
