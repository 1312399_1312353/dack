var express = require('express'),
    userRepo = require('../models/userRepo'),
    productRepo=require('../models/productRepo'),
    categoryRepo=require('../models/categoryRepo');
var r = express.Router();
function loaduserinfo(req, res,next){
    userRepo.loadinfo(req.params.id)
    .then(function(rows) {
            req.info= rows;
        //    res.send(rows==0);
            next();
    }).fail(function(err) {
        console.log(err);
        res.end('fail');
    });
}
function loadbidlist(req, res,next){
    userRepo.loadbidlist(req.params.id)
    .then(function(rows) {
            req.bidlist= rows;
        //    res.send(rows==0);
            next();
    }).fail(function(err) {
        console.log(err);
        res.end('fail');
    });
}
function loadwinlist(req, res,next){
    userRepo.loadwinlist(req.params.id)
    .then(function(rows) {
            req.winlist= rows;
        //    res.send(rows==0);
            next();
    }).fail(function(err) {
        console.log(err);
        res.end('fail');
    });
}
function loadselllist(req, res,next){
    userRepo.loadselllist(req.params.id)
    .then(function(rows) {
            req.selllist= rows;
        //    res.send(rows==0);
            next();
    }).fail(function(err) {
        console.log(err);
        res.end('fail');
    });
}
function renderselllist(req, res) {
    res.render('user/selllist', {
        layoutVM:  false,
        selllist: req.selllist,
        categories: req.category
    });
}
function loadpostlist(req, res,next){
    userRepo.loadpostlist(req.params.id)
    .then(function(rows) {
            req.postlist= rows;
            next();
    }).fail(function(err) {
        console.log(err);
        res.end('fail');
    });
}
function renderpostlist(req, res) {
    res.render('user/postlist', {
        layoutVM:  false,
        postlist: req.postlist,
        categories: req.category
    });
}
function loadAllCat(req, res,next) {
  categoryRepo.loadAll()
      .then(function(rows) {
              req.category= rows;
              next();
      }).fail(function(err) {
          console.log(err);
          res.end('fail');
      });
}
function loadAllUser(req, res,next) {
  userRepo.loadAll()
      .then(function(rows) {
              req.usermem= rows;
              next();
      }).fail(function(err) {
          console.log(err);
          res.end('fail');
      });
}
function renderUserPage(req, res) {
    res.render('user/user', {
        layoutVM:  false,
        categories: req.category,
        users:req.usermem
    });
}
function loadwatchlist(req, res,next){
    userRepo.loadwatchlist(req.params.id)
    .then(function(rows) {
            req.wpro= rows;
            next();
    }).fail(function(err) {
        console.log(err);
        res.end('fail');
    });
}
function loadratelist(req, res,next){
    userRepo.loadratelist(req.params.id)
    .then(function(rows) {
            req.ratehis= rows;
            next();
    }).fail(function(err) {
        console.log(err);
        res.end('fail');
    });
}
function renderratelist(req, res) {
    res.render('user/ratehistory', {
        layoutVM:  false,
        ratehis: req.ratehis,
        categories: req.category,
        users:req.usermem
    });

}
function renderwatchlist(req, res) {
    res.render('user/watchlist', {
        layoutVM:  false,
        products: req.wpro,
        categories: req.category,
        users:req.usermem
    });

}
function renderbidlist(req, res) {
    res.render('user/bidlist', {
        layoutVM:  false,
        bidlist:req.bidlist,
        categories: req.category
    });
}
function renderuserinfopage(req, res) {
    res.render('user/profile', {
        layoutVM:  false,
        categories: req.category
    });
}
r.post('/buy', function(req,res) {
    productRepo.bebuy(req.body);
    productRepo.hisinsert(req.body);
    res.back();
});
r.post('/profile/update', function(req,res) {
    userRepo.changepass(req.body);
    res.back();
});
function renderwinlist(req, res) {
    res.render('user/winlist', {
        layoutVM:  false,
        winlist:req.winlist,
        categories: req.category
    });
}
function renderpost(req, res) {
    res.render('user/post', {
        layoutVM:  false,
        categories: req.category
    });
}
r.post('/bit', function(req,res) {
    productRepo.bebit(req.body);
    productRepo.hisinsert(req.body);
    res.back();
});
r.get('/post',loadAllCat, renderpost);
r.get('/history/:id',loadAllCat,loadratelist,renderratelist);
r.get('/profile/:id',loadAllCat,renderuserinfopage);
r.get('/watchlist/:id',loadAllCat,loadwatchlist,renderwatchlist);
r.get('/selllist/:id',loadAllCat,loadselllist,renderselllist);
r.get('/postlist/:id',loadAllCat,loadpostlist,renderpostlist);
r.get('/winlist/:id',loadAllCat,loadwinlist,renderwinlist);
r.get('/bidlist/:id',loadAllCat,loadbidlist,renderbidlist);
r.get('/', loadAllCat,loadAllUser,renderUserPage);
r.post('/delete', function(req, res) {
    userRepo.delete(req.body).then(function(affectedRows) {
        res.redirect('/user');
    })
});
r.post('/reset', function(req, res) {
    userRepo.reset(req.body).then(function(affectedRows) {
        res.redirect('/user');
    })
});
r.post('/right', function(req, res) {
    userRepo.right(req.body).then(function(affectedRows) {
        res.redirect('/user');
    })
});
r.post('/addfavorite', function(req,res) {
    userRepo.insert(req.body);
    res.back();
});
r.post('/rate', function(req, res) {
  userRepo.insertratelist(req.body);
  userRepo.berate(req.body);
  res.back();
});
module.exports = r;
