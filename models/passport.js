var mustache = require('mustache'),
    db = require('../fn/db');
var LocalStrategy   = require('passport-local').Strategy;
// load up the user model
var mysql = require('mysql');
var dbconfig = require('../fn/dbconfig');
var connection = mysql.createConnection(dbconfig.connection);
var md5=require('md5');
connection.query('USE ' + dbconfig.database);
// expose this function to our app using module.exports
module.exports = function(passport) {
    passport.serializeUser(function(user, done) {
        done(null, user.uID);
    });
    passport.deserializeUser(function(id, done) {
        connection.query("SELECT * FROM user WHERE uID = ? ",[id], function(err, rows){
            done(err, rows[0]);
        });
    });
    passport.use(
        'local-register',
        new LocalStrategy({
            // by default, local strategy uses username and password, we will override with email
            usernameField : 'username',
            passwordField : 'password',
            passReqToCallback : true // allows us to pass back the entire request to the callback
        },
        function(req, username, password, done) {
            // find a user whose email is the same as the forms email
            // we are checking to see if the user trying to login already exists
            var isNameok=true;
            connection.query("SELECT * FROM user WHERE uName = ?",[username], function(err, rows) {
                if (err)
                    return done(err);
                if (rows.length) {
                //  console.log("nasd");
                  isNameok=false;
                    return done(null, false, req.flash('registerMessage', 'That username is already taken.'));
                }
            else
          {  connection.query("SELECT * FROM user WHERE uEmail = ?",[req.body.email], function(err, rows) {
                if (err)
                    return done(err);
                if (rows.length) {
                //  console.log("nasd");
                    return done(null, false, req.flash('registerMessage', 'That emmail is already taken.'));
                }
                else {
                  if (isNameok)
                    {var newUserMysql = {
                        username: username,
                        email: req.body.email,
                        type:0,
                        point:0,
                        password: md5(password)  // use the generateHash function in our user model
                    };
                    var insertQuery = "INSERT INTO user ( uName, uPassword,uEmail,uType,uPoint,IsRight ) values (?,?,?,?,?,0)";
                    connection.query(insertQuery,[newUserMysql.username, newUserMysql.password,newUserMysql.email,newUserMysql.type,newUserMysql.point],function(err, rows) {
                      if (err) return done(err);
                        newUserMysql.uID = rows.insertId;
                        console.log(newUserMysql.uID);
                        return done(null, newUserMysql);
                    });}
                }
            });}});
        })
    );
    passport.use(
        'local-login',
        new LocalStrategy({
            // by default, local strategy uses username and password, we will override with email
            usernameField : 'username',
            passwordField : 'password',
            passReqToCallback : true // allows us to pass back the entire request to the callback
        },
        function(req, username, password, done) { // callback with email and password from our form
            connection.query("SELECT * FROM user WHERE uName = ?",[username], function(err, rows){
                if (err)
                    return done(err);
                if (!rows.length) {
                    return done(null, false, req.flash('loginMessage', 'No user found.')); // req.flash is the way to set flashdata using connect-flash
                }
                else
                if (!(md5(password)==rows[0].uPassword))
                    return done(null, false, req.flash('loginMessage', 'Oops! Wrong password.')); // create the loginMessage and save it to session as flashdata
                return done(null, rows[0]);
            });

        })
    );
    
};
