var mustache = require('mustache'),
    db = require('../fn/db');
  q = require('q');
  md5=require('md5'),
  exports.loadAll = function() {
      var sql = 'select * from user';
      return db.load(sql);
  }
  exports.delete = function(entity) {
      var sql = mustache.render(
          'delete from user where uID = {{uID}}',
          entity
      );
      return db.delete(sql);
  }
  exports.reset = function(entity) {
      entity.password=md5("1");
      var sql = mustache.render(
          'update user set uPassword = "{{password}}" where uID = {{uID}}',
          entity
      );
      return db.update(sql);
  }
  exports.changepass = function(entity) {
      entity.uPassword=md5(entity.uPassword);
      var sql = mustache.render(
          'update user set uPassword = "{{uPassword}}" where uID = {{uID}}',
          entity
      );
      return db.update(sql);
  }
  exports.loadinfo=function(id) {
    var obj = {
        uID:id
    };
    var sql = mustache.render(
        'select * from user where uID="{{uID}}"',
        obj
    );
      return db.load(sql);
  }
  exports.right = function(entity) {
      var sql = mustache.render(
          'update user set IsRight = 1 where uID = {{uID}}',
          entity
      );

      return db.update(sql);
  }
  exports.insert = function(entity) {
      var sql = mustache.render(
          'insert into watchlist(uID,ProID,ProName,NowPrice) values("{{uID}}","{{ProID}}","{{ProName}}","{{NowPrice}}")',
          entity
      );
      return db.insert(sql);
  }
  exports.loadwatchlist = function(id) {
    var obj = {
        uID:id
    };
    var sql = mustache.render(
        'select * from watchlist where uID="{{uID}}"',
        obj
    );
      return db.load(sql);
  }
  exports.loadbidlist = function(id) {
    var obj = {
        uID:id
    };
    var sql = mustache.render(
        'SELECT p.ProID,p.Proname, p.Endtime, p.NowPrice, h.Price FROM products p, (select uID,ProID,Max(Price) as Price from bidhistory group by ProID) h WHERE p.ProID=h.ProID and h.uID="{{uID}}"',
        obj
    );
      return db.load(sql);
  }
  exports.loadwinlist = function(id) {
    var obj = {
        uID:id
    };
    var sql = mustache.render(
        'SELECT p.ProID,p.uID AS Seller,p.Proname, p.NowPrice, h.uID FROM products p, (select uID,ProID,Max(Price) as Price from bidhistory group by ProID) h  WHERE p.ProID=h.ProID and p.IsEnd=1 and p.Ownid="{{uID}}" and h.uID="{{uID}}" ',
        obj
    );
      return db.load(sql);
  }
  exports.insertratelist=function(entity) {
      var sql = mustache.render(
          'insert into ratelist(uID,uID2,uName,Point,Comment) values("{{uID}}","{{uID2}}","{{uName}}","{{Point}}","{{Comment}}")',
          entity
      );
      return db.insert(sql);
  }
  exports.berate = function(entity) {
      var sql = mustache.render(
          'update user set uPoint =uPoint+"{{Point}}",ratenum=ratenum+1 where uID = {{uID2}}',
          entity
      );
      return db.update(sql);
  }
  exports.loadratelist = function(id) {
    var obj = {
        uID:id
    };
    var sql = mustache.render(
        'select * from ratelist where uID2="{{uID}}"',
        obj
    );
      return db.load(sql);
  }
  exports.loadselllist=function(id) {
    var obj = {
        uID:id
    };
    var sql = mustache.render(
        'select * from products where uID="{{uID}}" and IsEnd=1',
        obj
    );
      return db.load(sql);
  }
  exports.loadpostlist=function(id) {
    var obj = {
        uID:id
    };
    var sql = mustache.render(
        'select * from products where uID="{{uID}}" and IsEnd=0',
        obj
    );
      return db.load(sql);
  }
  exports.updatedes= function(entity) {
      var sql = mustache.render(
          'update products set FullDes = "{{FullDes}}" where ProID = {{ProID}}',
          entity
      );
      return db.update(sql);
  }
