var mustache = require('mustache'),
    db = require('../fn/db');
  q = require('q'),
exports.loadAll = function() {
      var sql = 'select * from products';
      return db.load(sql);
}
exports.loadnotAll = function() {
      var sql = 'select * from products where IsConfirm=0 ORDER BY Starttime ASC';
      return db.load(sql);
}
exports.IsEnd=function() {
var sql = 'update products set IsEnd = 1 where Endtime<Now() or Sellprice=NowPrice';
return db.update(sql);
}
exports.bebuy=function(entity) {
  var sql = mustache.render(
      'update products set IsEnd = 1,NowPrice="{{Price}}",Ownid="{{uID}}" where ProID = "{{ProID}}"',
      entity
  );
  return db.update(sql);
}
exports.bebit=function(entity) {
  var sql = mustache.render(
      'update products set NowPrice="{{Price}}",Ownid="{{uID}}" where ProID = "{{ProID}}"',
      entity
  );
  return db.update(sql);
}
exports.hisinsert=function(entity) {
  var sql = mustache.render(
      'insert into bidhistory(ProID,uID,Price,uName,Time) values ("{{ProID}}","{{uID}}","{{Price}}","{{uName}}",Now()) ',
      entity
  );
  return db.update(sql);
}
exports.searchbyName= function(name) {
  var obj = {
      ProName: name
  };
  var sql =  mustache.render(
  'select * from products where ProName like "%{{ProName}}%"',
    obj );
  return db.load(sql);
}
exports.fixsearch= function(name,catID) {
  var obj = {
      ProName: name,
      CatID:catID
  };
  var sql =  mustache.render(
  'select * from products where ProName like "%{{ProName}}%" and CatID="{{CatID}}"',
    obj );
  return db.load(sql);
}
exports.loadAllByCat = function(catId) {
    var obj = {
        CatID: catId
    };
    var sql = mustache.render(
        'select * from products where CatID = {{CatID}} and IsEnd=0',
        obj
    );
    return db.load(sql);
}
exports.loadDetail=function(id){
  var d = q.defer();

    var obj = {
        ProID: id
    };
    var sql = mustache.render(
        'select * from products where ProID = {{ProID}}',
        obj
    );

    db.load(sql).then(function(rows) {
        d.resolve(rows[0]);
    });

    return d.promise;
}
exports.loadHistory=function(id){

    var obj = {
        ProID: id
    };
    var sql = mustache.render(
        'select * from bidhistory where ProID = {{ProID}}',
        obj
    );

    return db.load(sql);
}
exports.confirm = function(entity) {
  var sql = mustache.render(
      'update products set IsConfirm = 1 where ProID = {{ProID}}',
      entity
  );
  return db.update(sql);
}
exports.loadbyEndtime = function() {
    var sql = 'select * from products where IsEnd=0 ORDER BY Endtime DESC LIMIT 6 ';
    return db.load(sql);
}
exports.loadbyNumbit = function() {
    var sql = 'select * from products where IsEnd=0 ORDER BY Numbit DESC LIMIT 6';
    return db.load(sql);
}
exports.loadbyNowPrice = function() {
    var sql = 'select * from products where IsEnd=0 ORDER BY NowPrice DESC LIMIT 6';
    return db.load(sql);
}
exports.insert = function(entity) {
    var sql = mustache.render(
        'insert into products(ProName,CatID,Starttime,Sellprice,NowPrice,Endtime,uID,TinyDes,FullDes,IsAutotime,ProStep) values ("{{ProName}}","{{CatID}}",Now(),{{Sellprice}},{{NowPrice}},"{{Endtime}}",{{uID}},"{{TinyDes}}","{{FullDes}}","{{IsAutotime}}",{{ProStep}})',
        entity
    );
    return db.insert(sql);
}
